import express, { Request, Response, NextFunction } from 'express';
import { HttpError } from './error';
import { StructError } from 'superstruct';

import * as user from './requestHandlers/user';
import * as project from './requestHandlers/project';
import * as task from './requestHandlers/task';

const app = express();
const port = 3000;

app.use(express.json());

app.get("/users", user.get_all)
app.get("/users/:user_id", user.get_one)
app.post("/users", user.create_one)
app.patch("/users/:user_id", user.update_one)
app.delete("/users/:user_id", user.delete_one)

app.post("/users/:user_id/projects", project.create_one)
app.get("/users/:user_id/projects", project.get_one_of_users)

app.patch('/projects/:project_id', project.update_one)
app.delete('/projects/:project_id', project.delete_one)

app.post("/projects/:project_id/tasks", task.create_one)
app.get("/projects/:project_id/tasks", task.get_one_of_projects)

app.patch('/tasks/:task_id', task.update_one)
app.delete('/tasks/:task_id', task.delete_one)

app.get("/tasks/:task_id/assignees", task.get_users_of_task)
app.post("/tasks/:task_id/assignees/:user_id", task.create_one_on_project)

app.delete('/tasks/:task_id/assignees/:user_id', task.delete_one_on_project)

app.get("/users/:user_id/tasks", task.get_all_of_user)


app.use((err: HttpError, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof StructError) {
    err.status = 400;
    err.message = `Bad value for field ${err.key}`;
  }
  return res.status(err.status ?? 500).send(err.message);
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
