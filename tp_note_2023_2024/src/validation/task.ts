import { object, string, integer, date, boolean, min, optional, size, number } from 'superstruct';

export const TaskCreationData = object({
    title: size(string(), 1, 50),
    cost : number(),
    dueDate:  date(),
    completed : boolean(),
    project : optional(number())
  });

  export const TaskUpdatingData = object({
    title: optional(size(string(), 1, 50)),
    cost : optional(number()),
    dueDate:  optional(date()),
    completed : optional(boolean()),
    project : optional(number())

  })