import { object, string, optional, size, number } from 'superstruct';

export const ProjectCreationData = object({
    name: size(string(), 1, 50),
    description:  optional(size(string(), 1, 500)),
    creator : optional(number())
  });

  export const ProjectUpdatingData = object({
    name: optional(size(string(), 1, 50)),
    description:  optional(size(string(), 1, 500)),
    creator : optional(number())

  });