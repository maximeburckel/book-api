import { object, string, optional, size, refine } from 'superstruct';
import { isEmail } from 'validator';

   
export const UserCreationData = object({
    username: size(string(), 1, 50),
    email:  refine(size(string(), 1, 500), 'isEmail', (email) => isEmail(email)),
  });

  export const UserUpdatingData = object({
    username: optional(size(string(), 1, 50)),
    email:  optional(refine(size(string(), 1, 500), 'isEmail', (email) => isEmail(email))),
  });

