import { prisma } from '../db';
import type { Request, Response } from 'express';
import { HttpError } from '../error';
import { assert } from 'superstruct';
import {UserCreationData, UserUpdatingData} from '../validation/user'

export async function get_all(req: Request, res: Response) {
    const results = await prisma.user.findMany()
    if(results){
        res.status(200).json(results)
    }
    else{
        throw new HttpError("Users not found", 400)
    }
}

export async function get_one(req: Request, res: Response) {
    const results = await prisma.user.findUnique({
        where : {
            id : Number(req.params.user_id)
        }
    })
    if(results){
        res.status(200).json(results)
    }
    else{
        throw new HttpError("Users not found", 400)
    }
}

export async function create_one(req: Request, res: Response) {
    assert(req.body, UserCreationData, "Invalid Data")
    try{
        const results = await prisma.user.create({
            data : {...req.body}
            
        })
        
    }
    
    catch(error){
        throw new HttpError("Error during create", 400)
    }

}

export async function update_one(req: Request, res: Response) {
    assert(req.body, UserUpdatingData, "Invalid Data")
    try{
        const results = await prisma.user.update({
            where : {
                id : Number(req.params.user_id)
            },
            data : {...req.body}
        })
        if(results){
            res.status(200).json(results)
        }
    }
    catch(error){
        throw new HttpError("Users not found", 400)
    }
    
}

export async function delete_one(req: Request, res: Response) {
    
    const results = await prisma.user.delete({
        where : {
            id : Number(req.params.user_id)
        }
        
    })
    if(results){
        res.status(204).json(results)
    }
    else{
        throw new HttpError("Users not found", 400)
    }
}



