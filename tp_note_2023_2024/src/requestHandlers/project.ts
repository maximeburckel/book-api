import { prisma } from '../db';
import type { Request, Response } from 'express';
import { HttpError } from '../error';
import { assert } from 'superstruct';
import { ProjectCreationData,  ProjectUpdatingData} from '../validation/project';


export async function create_one(req: Request, res: Response) {
    assert(req.body, ProjectCreationData, "Invalid Data")
    try{
        const results = await prisma.project.create({
            data : {
                name : String(req.query.name),
                creator : Number(req.params.user_id),
                description : String(req.params.description)
            }
            
        })
        if (results){
            res.status(201).json(results)    
        }
        
    }
    
    catch(error){
        throw new HttpError("Error during create", 400)
    }

}

export async function get_one_of_users(req: Request, res: Response) {
    const results = await prisma.project.findMany({
        where : {
            user : {
                id : Number(req.params.user_id)
            }
        }
    })

    if(results){
        res.status(200).json(results)
    }
    else{
        throw new HttpError("Projects not found", 400)
    }

}

export async function update_one(req: Request, res: Response) {
    assert(req.body, ProjectUpdatingData, "Invalid Data")
    try{
        const results = await prisma.project.update({
            where : {
                id : Number(req.params.project_id)
            },
            data : {...req.body}
        })
        if(results){
            res.status(200).json(results)
        }
    }
    catch(error){
        throw new HttpError("Project not found", 400)
    }
    
}

export async function delete_one(req: Request, res: Response) {
    const results = await prisma.project.delete({
        where : {
            id : Number(req.params.project_id)
        }
        
    })
    if(results){
        res.status(204).json(results)
    }
    else{
        throw new HttpError("Project not found", 400)
    }
}
