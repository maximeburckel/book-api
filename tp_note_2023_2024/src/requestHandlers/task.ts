import { prisma } from '../db';
import { Prisma } from '@prisma/client';
import type { Request, Response } from 'express';
import { HttpError } from '../error';
import { assert } from 'superstruct';
import { TaskCreationData, TaskUpdatingData } from '../validation/task';




export async function create_one(req: Request, res: Response) {
    assert(req.body, TaskCreationData, "Invalid Data")
    try{
        const results = await prisma.task.create({
            data : {
                title: req.body.title,
    cost : req.body.cost,
    dueDate:  req.body.dueDate,
    completed : req.body.completed,
    projectId : Number(req.params.project_id)
            }
            
        })
        if (results){
            res.status(201).json(results)    
        }
        
    }
    
    catch(error){
        throw new HttpError("Error during create", 400)
    }

}

export async function get_one_of_projects(req: Request, res: Response) {
    const results = await prisma.task.findMany({
        where : {
            project : {
                id : Number(req.params.task_id)
            }
        }
    })

    if(results){
        res.status(200).json(results)
    }
    else{
        throw new HttpError("Tasks not found", 400)
    }

}

export async function update_one(req: Request, res: Response) {
    
    try{
        const results = await prisma.task.update({
            where : {
                id : Number(req.params.task_id)
            },
            data : req.body
        })
        if(results){
            res.status(200).json(results)
        }
    }
    catch(error){
        throw new HttpError("Task not found", 400)
    }
    
}

export async function delete_one(req: Request, res: Response) {
    const results = await prisma.task.delete({
        where : {
            id : Number(req.params.task_id)
        }
        
    })
    if(results){
        res.status(204).json(results)
    }
    else{
        throw new HttpError("Task not found", 400)
    }
}

export async function get_users_of_task(req: Request, res: Response) {
    const results = await prisma.user.findMany({
        where : {
            assignedTasks : {
                some : {
                    id : Number(req.params.task_id)
                }
            }
        }
        
    })
    if(results){
        res.status(200).json(results)
    }
    else{
        throw new HttpError("Task not found", 400)
    }
}

export async function create_one_on_project(req: Request, res: Response) {
    try{
        await prisma.task.update({
            where: {
              id: parseInt(req.params.task_id),
            },
            data: {
              project: {
                connect: {
                  id: parseInt(req.params.project_id),
                },
              },
            }
          })
          
    }
    catch(error){
        throw new HttpError("Error during create", 400)
    }
    res.status(200).send("Task successfully added")
}

export async function delete_one_on_project(req: Request, res: Response) {
    try{
        await prisma.project.update({
            where: {
              id: parseInt(req.params.task_id),
            },
            data: {
              tasks: {
                disconnect:  {
                     id: parseInt(req.params.project_id)
                }
              },
            }
          })
          
    }
    catch(error){
        throw new HttpError("Error during delete", 400)
    }
    res.status(200).send("Tag successfully removed on book")
}

export async function get_all_of_user(req: Request, res: Response) {
    const filter : Prisma.ProjectWhereInput = {
        user : {
            id: Number(req.params.user_id)
        }
    }
    if(req.query.completed === "true"){}
    try{
        await prisma.project.findMany({
            where: filter
            
          })
          
    }
    catch(error){
        throw new HttpError("Error during delete", 400)
    }
    res.status(200).send("Tag successfully removed on book")
}



