import { object, string, size, refine } from 'superstruct';
import validator from "validator";

export const UserCreationData = object({
    username: size(string(), 1, 50),
    email:  refine(size(string(), 1, 500), 'isEmail', (email) => validator.isEmail(email)),
    password: size(string(), 8, 50),
  });