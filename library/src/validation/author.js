"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthorCreationData = void 0;
const superstruct_1 = require("superstruct");
exports.AuthorCreationData = (0, superstruct_1.object)({
    firstname: (0, superstruct_1.size)((0, superstruct_1.string)(), 1, 50),
    lastname: (0, superstruct_1.size)((0, superstruct_1.string)(), 1, 50),
});
