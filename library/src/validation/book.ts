import { object, string, size } from 'superstruct';

export const BookCreationData = object({
  title: size(string(), 1, 50),
});
