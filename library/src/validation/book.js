"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookCreationData = void 0;
const superstruct_1 = require("superstruct");
exports.BookCreationData = (0, superstruct_1.object)({
    title: (0, superstruct_1.size)((0, superstruct_1.string)(), 1, 50),
});
