"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCreationData = void 0;
const superstruct_1 = require("superstruct");
const validator_1 = __importDefault(require("validator"));
exports.UserCreationData = (0, superstruct_1.object)({
    username: (0, superstruct_1.size)((0, superstruct_1.string)(), 1, 50),
    email: (0, superstruct_1.refine)((0, superstruct_1.size)((0, superstruct_1.string)(), 1, 500), 'isEmail', (email) => validator_1.default.isEmail(email)),
    password: (0, superstruct_1.size)((0, superstruct_1.string)(), 8, 50),
});
