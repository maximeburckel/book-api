"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TagCreationData = void 0;
const superstruct_1 = require("superstruct");
exports.TagCreationData = (0, superstruct_1.object)({
    name: (0, superstruct_1.size)((0, superstruct_1.string)(), 1, 50),
});
