"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.text = void 0;
exports.text = `
<style>
    .smallText {
      font-size: small;
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
      text-align: center;
      background-color: #f0f0f0;
      padding: 10px;
    }
  </style>
  <h1>Bienvenue sur mon API</h1>
  <h2>Voici la liste des routes : </h2>
  <h3>- Pour les auteurs : </h3>
  GET /authors (query possible : ?include=book )<br>
  GET /authors/:author_id (query possible : ?include=book )<br>
  DELETE /authors/:author_id<br>
  POST /authors<br>
  PATCH /authors/:author_id<br>
  <br>
  <h3>- Pour les livres : </h3>
  GET /books (query possible : ?include=author )<br>
  GET /books/:book_id (query possible : ?include=author )<br>
  DELETE /books/:book_id<br>
  GET /authors/:author_id/books (query possible : ?include=author ) <br>
  POST /authors/:author_id/books<br>
  PATCH /books/:book_id<br>
  <br>
  <h3>- Pour les tags : </h3>
  GET /tags <br>
  GET /tags/:tag_id <br>
  POST /tags <br>
  GET /books/:book_id/tags <br>
  DELETE /tags/:tag_id<br>
  DELETE /books/:book_id/tags/:tag_id
  POST /books/:book_id/tags/:tag_id<br>
  PATCH /tags/:tag_id<br>
  <div class="smallText">Cette API a été invoqué par Moloch</div>
  `;
