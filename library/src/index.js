"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const author = __importStar(require("./requestHandlers/author"));
const book = __importStar(require("./requestHandlers/book"));
const tag = __importStar(require("./requestHandlers/tag"));
const user = __importStar(require("./requestHandlers/user"));
const express_1 = __importDefault(require("express"));
const superstruct_1 = require("superstruct");
const home_1 = require("./home");
const app = (0, express_1.default)();
const port = 3000;
const cors = require('cors');
//middleware pour le json
app.use(express_1.default.json());
// ...
// ...
app.use(cors());
app.get('/', (req, res) => {
    res.send(home_1.text);
});
//Authors
app.get('/authors', author.get_all);
app.get('/authors/:author_id', author.get_one);
app.delete('/authors/:author_id', author.delete_one);
app.post('/authors', author.create_one);
app.patch('/authors/:author_id', author.update_one);
//Books
app.get('/books', book.get_all);
app.get('/books/:book_id', book.get_one);
app.get('/authors/:author_id/books', book.get_all_of_author);
app.post('/authors/:author_id/books', book.create_one_of_author);
app.patch('/books/:book_id', book.update_one);
app.delete('/books/:book_id', book.delete_one);
//Tags
app.get('/tags', tag.get_all);
app.get('/tags/:tag_id', tag.get_one);
app.get('/books/:book_id/tags', tag.get_all_of_book);
app.post("/tags", tag.create_one);
app.patch('/tags/:tag_id', tag.update_one);
app.delete('/tags/:tag_id', tag.delete_one);
app.delete('/books/:book_id/tags/:tag_id', tag.delete_one_on_book);
app.post('/books/:book_id/tags/:tag_id', tag.create_one_on_book);
//user
app.post('/signup', user.create_one);
//Middleware de gestion des erreurs
app.use((err, req, res, next) => {
    var _a;
    if (err instanceof superstruct_1.StructError) {
        err.status = 400;
        err.message = String(`Bad value for field ${err.key}`);
    }
    res.status((_a = err.status) !== null && _a !== void 0 ? _a : 500).send(err.message);
});
//lancement du serveur
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
