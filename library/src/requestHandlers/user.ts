import express, { Request, Response, NextFunction } from 'express'
import {prisma} from "../db"
import { assert, number, object,  string, StructError } from 'superstruct'
import { HttpError } from "./../error"
import { UserCreationData } from '../validation/user';
const bcrypt = require('bcryptjs');
import {Prisma} from "@prisma/client"

export async function create_one(req: Request, res: Response){
    //console.log(req.body.email)
    assert(req.body, UserCreationData, "Data are Invalid");
    try{
        await prisma.user.create({
            data: {
                username : req.body.username,
                password : String(bcrypt.hash(req.body.password, 256)),
                email : req.body.email
            }
        })
    }

    catch(error : any){
        console.log(error.message);
        throw new HttpError("Error during create", 400)
    }
    
    
   
    res.status(201).send("User successfully added")
}


// export async function signin(req: Request, res: Response){
//     //console.log(req.body.email)
//     const filter : Prisma.UserWhereInput = {}
//     filter.username = req.body.username
//     assert(req.body, UserCreationData, "Data are Invalid");
//     const results = await prisma.user.findFirst({
//         where : filter
//     })
//     if(results){
//         if(bcrypt.compare(req.body.password, results.password)){
//             await prisma.user.update({
//                 data : {
//                     password : 
//                 },
//                 where : filter
//             })
//         }
//     }
    
    
    
   
    //res.status(201).send("User successfully added")
//}

