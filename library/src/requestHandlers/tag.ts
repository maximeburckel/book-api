import express, { Request, Response, NextFunction } from 'express'
import {prisma} from "../db"
import { assert, number, object,  string, StructError } from 'superstruct'
import { HttpError } from "./../error"
import { TagCreationData } from '../validation/tag';

export async function get_all(req: Request, res: Response) {
    const results = await prisma.tag.findMany({
        include :{
            books : true
        }
    })
    if(!results || results.length == 0){
        throw new HttpError("Tag not found", 400)
    }
    else{
        res.status(200).json(results)
    }
}

export async function get_one(req: Request, res: Response) {
    const results = await prisma.tag.findFirst({
        where : {
            id : parseInt(req.params.tag_id)
        },
        include : {
            books : true
        }
    })
    if(!results){
        throw new HttpError("Tag not found", 400)
    }
    else{
        res.status(200).json(results)
    }
}

export async function get_all_of_book(req: Request, res: Response){
    const results = await prisma.tag.findMany({
        where  : {
            books : {
                some : {
                    id : parseInt(req.params.book_id)
                }
                
            }
        },
        include :{
            books : true
        }
    })
    if(!results || results.length == 0){
        throw new HttpError("Tags not found", 500)
    }
    else{
        res.status(200).json(results)
    }
}

export async function create_one(req: Request, res: Response){
    assert(req.body, TagCreationData, "Data are Invalid");
    
    try{
        await prisma.tag.create({
            data: {
                name : req.body.name,
            }
        })
    }

    catch(error){
        throw new HttpError("Error during create", 400)
    }
    
    
   
    res.status(201).send("Tag successfully added")
}

export async function update_one(req: Request, res: Response) {
    try{
        await prisma.tag.update({
            where : {
                id : parseInt(req.params.tag_id)
            },
            data : req.body
        })
    } catch(error){
        throw new HttpError("Error during update", 400)
    }
    
    
    
    res.status(200).send("Tag successfully updated")
    
}

export async function delete_one(req: Request, res: Response) {
    try{
        await prisma.tag.delete({
            where : {
                id : parseInt(req.params.tag_id)
            }
        })
    } catch(error){
        throw new HttpError("Error during delete", 400)
    }
    
    
    
    res.status(200).send("Tag successfully deleted")
    
}

export async function create_one_on_book(req: Request, res: Response) {
    try{
        await prisma.tag.update({
            where: {
              id: parseInt(req.params.tag_id),
            },
            data: {
              books: {
                connect: {
                  id: parseInt(req.params.book_id),
                },
              },
            },
            include: {
              books: true,
            },
          })
          
    }
    catch(error){
        throw new HttpError("Error during create", 400)
    }
    res.status(200).send("Tag successfully added")
}

export async function delete_one_on_book(req: Request, res: Response) {
    try{
        await prisma.tag.update({
            where: {
              id: parseInt(req.params.tag_id),
            },
            data: {
              books: {
                disconnect:  {
                     id: parseInt(req.params.book_id)
                }
              },
            },
            include: {
              books: true,
            },
          })
          
    }
    catch(error){
        throw new HttpError("Error during delete", 400)
    }
    res.status(200).send("Tag successfully removed on book")
}