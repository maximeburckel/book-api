import express, { Request, Response, NextFunction } from 'express'
import {prisma} from "../db"
import { assert } from 'superstruct'
import { HttpError } from "./../error"
import {Prisma} from "@prisma/client"
import { BookCreationData } from '../validation/book';




export async function get_all(req: Request, res: Response) {
    const whereCondition : Prisma.BookWhereInput = {};
    const selectCondition : Prisma.BookInclude = {};
    if(req.query.include === "author"){
        selectCondition.author = {
                select: { id : true, firstname: true, lastname : true },
        } 
    }
    
    if(req.query.title){
        whereCondition.title = {
            contains: String(req.query.title)
        }
    }


    let skip = parseInt(String(req.query.skip));
    let take = parseInt(String(req.query.take));

    if (isNaN(skip) || isNaN(take)) {
        skip = 0,
        take = 50
    }

    const results = await prisma.book.findMany({
        skip : skip,
        take : take,
        where : whereCondition,
        orderBy: {
            title: 'asc'
          },
        include: selectCondition
    })

    if(!results || results.length == 0){
        throw new HttpError("Books not found", 400)
    }
    else{
        const totalCount = await prisma.book.count({
            where: whereCondition,
        })       
        res.set('X-Total-Count', String(totalCount)).status(200).json(results)
    }
}

export async function get_one(req: Request, res: Response) {
    const selectCondition : Prisma.BookInclude = {};
    if(req.query.include === "author"){
        selectCondition.author = {
                select: { id : true, firstname: true, lastname : true },
        } 
    }
    const results = await prisma.book.findFirst({
        where : {
            id : parseInt(req.params.book_id)
        },
        include: selectCondition
    })
    if(!results){
        throw new HttpError("Book not found", 400)
    }
    else{
        res.status(200).json(results)
    }
}

export async function get_all_of_author(req: Request, res: Response) {
    const whereCondition : Prisma.BookWhereInput= {};
    const selectCondition : Prisma.BookInclude = {};
    if(req.query.include === "author"){
        selectCondition.author = {
                select: { id : true, firstname: true, lastname : true },
        } 
    }

    if (req.query.title) {
        whereCondition.authorId = parseInt(req.params.author_id) 
        
        whereCondition.title = {
            contains: String(req.query.title)
        }
    }
    else{
        whereCondition.authorId = parseInt(req.params.author_id)
    }

    let skip = parseInt(String(req.query.skip));
    let take = parseInt(String(req.query.take));

    if (isNaN(skip) || isNaN(take)) {
        skip = 0,
        take = 40
      }

    const results = await prisma.book.findMany({
        skip : skip,
        take : take,
        where : whereCondition,
        orderBy : {
            title : "asc"
        },
        include: selectCondition
    })
    if(!results || results.length == 0){
        throw new HttpError("Books not found", 500)
    }
    else{
        const totalCount = await prisma.book.count({
            where: whereCondition,
        })       
        res.set('X-Total-Count', String(totalCount)).status(200).json(results)
    }
}

export async function update_one(req: Request, res: Response) {
    try{
        await prisma.book.update({
            where : {
                id : parseInt(req.params.book_id)
            },
            data : req.body
        })
    } catch(error){
        throw new HttpError("Error during update", 400)
    }
    
    
    
    res.status(200).send("Book successfully updated")
    
}

export async function delete_one(req: Request, res: Response) {
    try{
        await prisma.book.delete({
            where : {
                id : parseInt(req.params.book_id)
            }
        })
    } catch(error){
        throw new HttpError("Error during delete", 400)
    }
    
    
    res.status(204).send("Book successfully deleted")
    
}

export async function create_one_of_author(req: Request, res: Response) {
    assert(req.body, BookCreationData, "Data are Invalid")
    await prisma.book.create({
            data: {
                title: req.body.title,
                authorId: parseInt(req.params.author_id),
            }
    })
    

    res.status(201).send("Book successfully added")
    
}