"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.delete_one_on_book = exports.create_one_on_book = exports.delete_one = exports.update_one = exports.create_one = exports.get_all_of_book = exports.get_one = exports.get_all = void 0;
const db_1 = require("../db");
const superstruct_1 = require("superstruct");
const error_1 = require("./../error");
const tag_1 = require("../validation/tag");
function get_all(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield db_1.prisma.tag.findMany({
            include: {
                books: true
            }
        });
        if (!results || results.length == 0) {
            throw new error_1.HttpError("Tag not found", 400);
        }
        else {
            res.status(200).json(results);
        }
    });
}
exports.get_all = get_all;
function get_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield db_1.prisma.tag.findFirst({
            where: {
                id: parseInt(req.params.tag_id)
            },
            include: {
                books: true
            }
        });
        if (!results) {
            throw new error_1.HttpError("Tag not found", 400);
        }
        else {
            res.status(200).json(results);
        }
    });
}
exports.get_one = get_one;
function get_all_of_book(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield db_1.prisma.tag.findMany({
            where: {
                books: {
                    some: {
                        id: parseInt(req.params.book_id)
                    }
                }
            },
            include: {
                books: true
            }
        });
        if (!results || results.length == 0) {
            throw new error_1.HttpError("Tags not found", 500);
        }
        else {
            res.status(200).json(results);
        }
    });
}
exports.get_all_of_book = get_all_of_book;
function create_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        (0, superstruct_1.assert)(req.body, tag_1.TagCreationData, "Data are Invalid");
        try {
            yield db_1.prisma.tag.create({
                data: {
                    name: req.body.name,
                }
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during create", 400);
        }
        res.status(201).send("Tag successfully added");
    });
}
exports.create_one = create_one;
function update_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.tag.update({
                where: {
                    id: parseInt(req.params.tag_id)
                },
                data: req.body
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during update", 400);
        }
        res.status(200).send("Tag successfully updated");
    });
}
exports.update_one = update_one;
function delete_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.tag.delete({
                where: {
                    id: parseInt(req.params.tag_id)
                }
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during delete", 400);
        }
        res.status(200).send("Tag successfully deleted");
    });
}
exports.delete_one = delete_one;
function create_one_on_book(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.tag.update({
                where: {
                    id: parseInt(req.params.tag_id),
                },
                data: {
                    books: {
                        connect: {
                            id: parseInt(req.params.book_id),
                        },
                    },
                },
                include: {
                    books: true,
                },
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during create", 400);
        }
        res.status(200).send("Tag successfully added");
    });
}
exports.create_one_on_book = create_one_on_book;
function delete_one_on_book(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.tag.update({
                where: {
                    id: parseInt(req.params.tag_id),
                },
                data: {
                    books: {
                        disconnect: {
                            id: parseInt(req.params.book_id)
                        }
                    },
                },
                include: {
                    books: true,
                },
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during delete", 400);
        }
        res.status(200).send("Tag successfully removed on book");
    });
}
exports.delete_one_on_book = delete_one_on_book;
