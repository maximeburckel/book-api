"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.delete_one = exports.update_one = exports.create_one = exports.get_one = exports.get_all = void 0;
const db_1 = require("../db");
const superstruct_1 = require("superstruct");
const error_1 = require("./../error");
const author_1 = require("../validation/author");
function get_all(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const whereCondition = {};
        const selectCondition = {};
        if (req.query.include === "book") {
            selectCondition.books = {
                select: { id: true, title: true },
                orderBy: {
                    title: "asc"
                }
            };
        }
        if (req.query.hasBooks === "true") {
            whereCondition.books = {
                some: {}
            };
        }
        if (req.query.skip && req.query.take) {
            selectCondition._count = {
                select: { books: true }
            };
        }
        // _count: {
        //     select: { books: true },
        // },
        if (req.query.lastname) {
            whereCondition.lastname = {
                contains: String(req.query.lastname)
            };
        }
        let skip = parseInt(String(req.query.skip));
        let take = parseInt(String(req.query.take));
        if (isNaN(skip) || isNaN(take)) {
            skip = 0,
                take = 40;
        }
        const results = yield db_1.prisma.author.findMany({
            skip: skip,
            take: take,
            where: whereCondition,
            include: selectCondition,
            orderBy: {
                lastname: 'asc'
            },
        });
        if (results.length == 0 || !results) {
            throw new error_1.HttpError('Authors not found ', 500);
        }
        else {
            const totalCount = yield db_1.prisma.author.count({
                where: whereCondition,
            });
            res.set('X-Total-Count', String(totalCount)).status(200).json(results);
        }
    });
}
exports.get_all = get_all;
;
function get_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const selectCondition = {};
        if (req.query.include === "book") {
            selectCondition.books = {
                select: { id: true, title: true },
                orderBy: {
                    title: "asc"
                }
            };
        }
        const results = yield db_1.prisma.author.findFirst({
            where: {
                id: parseInt(req.params.author_id)
            },
            include: selectCondition
        });
        if (!results) {
            throw new error_1.HttpError('Author not found ', 500);
        }
        else {
            res.status(200).json(results);
        }
    });
}
exports.get_one = get_one;
;
function create_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        (0, superstruct_1.assert)(req.body, author_1.AuthorCreationData, "Data are Invalid");
        yield db_1.prisma.author.create({
            data: req.body
        });
        res.status(201).send("Author successfully added");
    });
}
exports.create_one = create_one;
;
function update_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.author.update({
                where: {
                    id: parseInt(req.params.author_id)
                },
                data: req.body
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during update", 400);
        }
        res.status(200).send("Authors updated");
    });
}
exports.update_one = update_one;
;
function delete_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.author.delete({
                where: {
                    id: parseInt(req.params.author_id)
                }
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during delete", 400);
        }
        res.status(204).send("Author successfully deleted");
    });
}
exports.delete_one = delete_one;
;
