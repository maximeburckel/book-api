import {prisma} from "../db"
import express, { Request, Response } from 'express'
import { assert, boolean} from 'superstruct'
import { HttpError } from "./../error"
import {Prisma} from "@prisma/client"
import { AuthorCreationData } from '../validation/author';



export async function get_all(req: Request, res: Response) {
    const whereCondition : Prisma.AuthorWhereInput = {};
    const selectCondition: Prisma.AuthorInclude = {};
    if (req.query.include === "book") {
        selectCondition.books = {
            select: { id: true, title: true },
            orderBy: {
                title: "asc"
            }
        }            
    }

    if(req.query.hasBooks === "true"){
        whereCondition.books = {
            some : {
                
            }
            
        }
    }

    if(req.query.skip && req.query.take){
        selectCondition._count = {
            select: { books: true }
        }
    }
     // _count: {
            //     select: { books: true },
            // },
    
    
    if(req.query.lastname){
        whereCondition.lastname = {
             contains: String(req.query.lastname) 
        }
    }

    let skip = parseInt(String(req.query.skip));
    let take = parseInt(String(req.query.take));

    if (isNaN(skip) || isNaN(take)) {
        skip = 0,
        take = 40
      }

    const results = await prisma.author.findMany({
        skip : skip,
        take : take,
        where: whereCondition,
          include: selectCondition,
          orderBy: {
            lastname: 'asc'
          },
        
    })
    
    if(results.length == 0 || !results){
        throw new HttpError('Authors not found ', 500);
    }
    else{
         const totalCount = await prisma.author.count({
            where: whereCondition,
        })       
        res.set('X-Total-Count', String(totalCount)).status(200).json(results)
    }
};

export async function get_one(req: Request, res: Response) {
    const selectCondition: Prisma.AuthorInclude = {};
    if (req.query.include === "book") {
        selectCondition.books = {
            select: { id: true, title: true },
            orderBy: {
                title: "asc"
            }
        }            
    }


    const results = await prisma.author.findFirst({
        where : {
            id : parseInt(req.params.author_id)
        },
        include: selectCondition
    })
    if(!results){
        throw new HttpError('Author not found ', 500);
    }
    else{
        res.status(200).json(results)
    }
};

export async function create_one(req: Request, res: Response) {
    
    assert(req.body, AuthorCreationData, "Data are Invalid");
    
    await prisma.author.create({
        data: req.body
    })
    
   
    res.status(201).send("Author successfully added")
    
};

export async function update_one(req: Request, res: Response) {
    try{
        await prisma.author.update({
            where : {
                id : parseInt(req.params.author_id)
            },
            data : req.body
        })
    }
    catch(error){
        throw new HttpError("Error during update", 400)
    }
    
    
    res.status(200).send("Authors updated")
       
    
   
};

export async function delete_one(req: Request, res: Response) {
    try{
    await prisma.author.delete({
        where : {
                id : parseInt(req.params.author_id)
        }
    })
    }catch(error){
        throw new HttpError("Error during delete", 400)
    }
    

    res.status(204).send("Author successfully deleted")
};
  