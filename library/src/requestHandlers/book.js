"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.create_one_of_author = exports.delete_one = exports.update_one = exports.get_all_of_author = exports.get_one = exports.get_all = void 0;
const db_1 = require("../db");
const superstruct_1 = require("superstruct");
const error_1 = require("./../error");
const book_1 = require("../validation/book");
function get_all(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const whereCondition = {};
        const selectCondition = {};
        if (req.query.include === "author") {
            selectCondition.author = {
                select: { id: true, firstname: true, lastname: true },
            };
        }
        if (req.query.title) {
            whereCondition.title = {
                contains: String(req.query.title)
            };
        }
        let skip = parseInt(String(req.query.skip));
        let take = parseInt(String(req.query.take));
        if (isNaN(skip) || isNaN(take)) {
            skip = 0,
                take = 50;
        }
        const results = yield db_1.prisma.book.findMany({
            skip: skip,
            take: take,
            where: whereCondition,
            orderBy: {
                title: 'asc'
            },
            include: selectCondition
        });
        if (!results || results.length == 0) {
            throw new error_1.HttpError("Books not found", 400);
        }
        else {
            const totalCount = yield db_1.prisma.book.count({
                where: whereCondition,
            });
            res.set('X-Total-Count', String(totalCount)).status(200).json(results);
        }
    });
}
exports.get_all = get_all;
function get_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const selectCondition = {};
        if (req.query.include === "author") {
            selectCondition.author = {
                select: { id: true, firstname: true, lastname: true },
            };
        }
        const results = yield db_1.prisma.book.findFirst({
            where: {
                id: parseInt(req.params.book_id)
            },
            include: selectCondition
        });
        if (!results) {
            throw new error_1.HttpError("Book not found", 400);
        }
        else {
            res.status(200).json(results);
        }
    });
}
exports.get_one = get_one;
function get_all_of_author(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const whereCondition = {};
        const selectCondition = {};
        if (req.query.include === "author") {
            selectCondition.author = {
                select: { id: true, firstname: true, lastname: true },
            };
        }
        if (req.query.title) {
            whereCondition.authorId = parseInt(req.params.author_id);
            whereCondition.title = {
                contains: String(req.query.title)
            };
        }
        else {
            whereCondition.authorId = parseInt(req.params.author_id);
        }
        let skip = parseInt(String(req.query.skip));
        let take = parseInt(String(req.query.take));
        if (isNaN(skip) || isNaN(take)) {
            skip = 0,
                take = 40;
        }
        const results = yield db_1.prisma.book.findMany({
            skip: skip,
            take: take,
            where: whereCondition,
            orderBy: {
                title: "asc"
            },
            include: selectCondition
        });
        if (!results || results.length == 0) {
            throw new error_1.HttpError("Books not found", 500);
        }
        else {
            const totalCount = yield db_1.prisma.book.count({
                where: whereCondition,
            });
            res.set('X-Total-Count', String(totalCount)).status(200).json(results);
        }
    });
}
exports.get_all_of_author = get_all_of_author;
function update_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.book.update({
                where: {
                    id: parseInt(req.params.book_id)
                },
                data: req.body
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during update", 400);
        }
        res.status(200).send("Book successfully updated");
    });
}
exports.update_one = update_one;
function delete_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield db_1.prisma.book.delete({
                where: {
                    id: parseInt(req.params.book_id)
                }
            });
        }
        catch (error) {
            throw new error_1.HttpError("Error during delete", 400);
        }
        res.status(204).send("Book successfully deleted");
    });
}
exports.delete_one = delete_one;
function create_one_of_author(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        (0, superstruct_1.assert)(req.body, book_1.BookCreationData, "Data are Invalid");
        yield db_1.prisma.book.create({
            data: {
                title: req.body.title,
                authorId: parseInt(req.params.author_id),
            }
        });
        res.status(201).send("Book successfully added");
    });
}
exports.create_one_of_author = create_one_of_author;
