"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.create_one = void 0;
const db_1 = require("../db");
const superstruct_1 = require("superstruct");
const error_1 = require("./../error");
const user_1 = require("../validation/user");
const bcrypt = require('bcryptjs');
function create_one(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        //console.log(req.body.email)
        (0, superstruct_1.assert)(req.body, user_1.UserCreationData, "Data are Invalid");
        try {
            yield db_1.prisma.user.create({
                data: {
                    username: req.body.username,
                    password: String(bcrypt.hash(req.body.password, 256)),
                    email: req.body.email
                }
            });
        }
        catch (error) {
            console.log(error.message);
            throw new error_1.HttpError("Error during create", 400);
        }
        res.status(201).send("User successfully added");
    });
}
exports.create_one = create_one;
// export async function signin(req: Request, res: Response){
//     //console.log(req.body.email)
//     const filter : Prisma.UserWhereInput = {}
//     filter.username = req.body.username
//     assert(req.body, UserCreationData, "Data are Invalid");
//     const results = await prisma.user.findFirst({
//         where : filter
//     })
//     if(results){
//         if(bcrypt.compare(req.body.password, results.password)){
//             await prisma.user.update({
//                 data : {
//                     password : 
//                 },
//                 where : filter
//             })
//         }
//     }
//res.status(201).send("User successfully added")
//}
