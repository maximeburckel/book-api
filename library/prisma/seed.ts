import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient();

const books = [
    {
      title: 'H. P. 2',
      authorId: 1
    },
    
  ];


  

async function main() {  
    // Créer un utilisateur avec des commentaires et des évaluations associés
    const user = await prisma.user.create({
      data: {
        email: 'user@example.com',
        password: 'password123',
        username: 'example_user',
        comments: {
          create: [
            {
              content: 'Ceci est un commentaire sur le livre.',
              createdAt: new Date(),
              updatedAt: new Date(),
              book: {
                create: {
                  title: 'Titre du livre',
                  author: {
                    create: {
                      firstname: 'John',
                      lastname: 'Doe',
                    },
                  },
                },
              },
            },
          ],
        },
        rates: {
          create: [
            {
              value: 5,
              book: {
                create: {
                  title: 'Autre livre',
                  author: {
                    create: {
                      firstname: 'Jane',
                      lastname: 'Doe',
                    },
                  },
                },
              },
            },
          ],
        },
      },
    });
  

}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect();
    process.exit(1)
  });
